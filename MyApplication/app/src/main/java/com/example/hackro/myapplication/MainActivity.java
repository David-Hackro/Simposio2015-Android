package com.example.hackro.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    EditText User,Password;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        User = (EditText)findViewById(R.id.editText);
        Password = (EditText)findViewById(R.id.editText2);

    }

    public void Autenticacion(View view){
        if(User.getText().toString().equals("hackro") && Password.getText().toString().equals("12345")){
                Intent intent = new Intent(this,Main2Activity.class);
                startActivity(intent);
        }
        else {
            Toast.makeText(this,"Usuario Invalido",Toast.LENGTH_LONG).show();
        }
    }
}
